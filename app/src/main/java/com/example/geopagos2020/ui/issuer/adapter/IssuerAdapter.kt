package com.example.geopagos2020.ui.issuer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.geopagos2020.R
import com.example.geopagos2020.model.Issuer
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_issuer.view.*

class IssuerAdapter(
    private var context: Context,
    private val issuers: ArrayList<Issuer>
) : RecyclerView.Adapter<IssuerAdapter.ViewHolder>() {

    private lateinit var onClickListener: IOnClickListener

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgBank: ImageView = itemView.img_bank
        val bankName: TextView = itemView.tv_bankName
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.with(context).load(issuers[position].secure_thumbnail)
            .into(holder.imgBank)
        holder.bankName.text = issuers[position].name
        holder.bankName.setOnClickListener {
            onClickListener.OnItemClick(position)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_issuer, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return issuers.size
    }

    fun setOnClick(onClick: IOnClickListener) {
        onClickListener = onClick
    }

    interface IOnClickListener {
        fun OnItemClick(position: Int)
    }

}