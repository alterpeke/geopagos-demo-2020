package com.example.geopagos2020.ui.installment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.geopagos2020.model.Installment
import com.example.geopagos2020.repository.PaymentRepository
import com.example.geopagos2020.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class InstallmentViewModel(private val paymentRepository: PaymentRepository) : ViewModel() {

    private val installmentsLiveData = MutableLiveData<Resource<ArrayList<Installment>>>()
    private val compositeDisposable = CompositeDisposable()

    fun init(publickey: String, amount: String, paymentMethodId: String, issuerId: String) {
        fetchInstallments(publickey, amount, paymentMethodId, issuerId)
    }

    private fun fetchInstallments(
        publickey: String,
        amount: String,
        paymentMethodId: String,
        issuerId: String
    ) {
        installmentsLiveData.postValue(Resource.loading(null))
        compositeDisposable.add(
            paymentRepository.getInstallment(publickey, amount, paymentMethodId, issuerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ paymentMethod ->
                    installmentsLiveData.postValue(Resource.success(paymentMethod))
                }, { throwable ->
                    installmentsLiveData.postValue(Resource.error(null, "Something Went Wrong"))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getInstallments(): LiveData<Resource<ArrayList<Installment>>> {
        return installmentsLiveData
    }

}