package com.example.geopagos2020.ui.method.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.geopagos2020.R
import com.example.geopagos2020.model.PaymentMethod
import kotlinx.android.synthetic.main.item_payment_method.view.*

class PaymentMethodAdapter(
    private val context: Context,
    private val paymentMethods: ArrayList<PaymentMethod>
) : RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>() {

    private lateinit var onClickListener: IOnClickListener

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView = itemView.tv_name
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvTitle.text = paymentMethods.get(position).name
        holder.tvTitle.setOnClickListener {
            onClickListener.OnItemClick(position)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_payment_method, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return paymentMethods.size
    }

    fun setOnClick(onClick: IOnClickListener) {
        onClickListener = onClick
    }

    interface IOnClickListener {
        fun OnItemClick(position: Int)
    }

}