package com.example.geopagos2020.ui.installment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.geopagos2020.R
import com.example.geopagos2020.model.util.PaymentData
import com.example.geopagos2020.ui.installment.adapter.InstallmentAdapter
import com.example.geopagos2020.utils.Status
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.android.ext.android.inject

class InstallmentFragment : Fragment() {

    private val viewModel: InstallmentViewModel by inject()
    private lateinit var adapter: InstallmentAdapter
    private var data: PaymentData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            if (bundle.containsKey("DATA")) {
                data = bundle.getParcelable("DATA")
            }
        }
        data?.let {
            viewModel.init(
                getString(R.string.publicKey),
                it.amount.orEmpty(),
                it.paymentMethodId.orEmpty(),
                it.issuerId.orEmpty()
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getInstallments().observe(viewLifecycleOwner, Observer { ob ->

            when (ob.status) {
                Status.SUCCESS -> {
                    ob.data?.let {
                        progressBar.visibility = View.GONE
                        adapter = InstallmentAdapter(requireActivity(), it[0].payerCosts)
                        adapter.setOnClick(object : InstallmentAdapter.IOnClickListener {
                            override fun OnItemClick(position: Int) {
                                data?.installmentTitle = it[0].payerCosts[position].recommended_message
                                data?.installment = it[0].payerCosts[position].installments.toString()
                                val bundle = bundleOf("DATA" to data)
                                findNavController().navigate(
                                    R.id.action_installmentFragment_to_fragmentRoot,
                                    bundle
                                )
                            }
                        })
                        rv_list.setHasFixedSize(true)
                        rv_list.layoutManager = LinearLayoutManager(activity)
                        rv_list.adapter = adapter
                    }

                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    rv_list.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        "Error on loading Installments",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

}