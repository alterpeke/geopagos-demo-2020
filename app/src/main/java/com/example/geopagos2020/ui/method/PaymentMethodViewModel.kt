package com.example.geopagos2020.ui.method

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.geopagos2020.model.PaymentMethod
import com.example.geopagos2020.repository.PaymentRepository
import com.example.geopagos2020.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PaymentMethodViewModel(private val paymentRepository: PaymentRepository) : ViewModel(){

    private val paymentMethodLiveData = MutableLiveData<Resource<ArrayList<PaymentMethod>>>()
    private val compositeDisposable = CompositeDisposable()

    fun init(token: String){
        fetchPaymentMethods(token)
    }

    private fun fetchPaymentMethods(token: String) {
        paymentMethodLiveData.postValue(Resource.loading(null))
        compositeDisposable.add(
            paymentRepository.getPaymentMethods(token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ paymentMethod ->
                    paymentMethodLiveData.postValue(Resource.success(paymentMethod))
                }, { throwable ->
                    paymentMethodLiveData.postValue(Resource.error(null, "Something Went Wrong"))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getPaymentMethods(): LiveData<Resource<ArrayList<PaymentMethod>>> {
        return paymentMethodLiveData
    }

}