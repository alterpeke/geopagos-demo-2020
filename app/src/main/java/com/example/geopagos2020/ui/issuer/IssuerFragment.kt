package com.example.geopagos2020.ui.issuer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.geopagos2020.R
import com.example.geopagos2020.model.util.PaymentData
import com.example.geopagos2020.ui.issuer.adapter.IssuerAdapter
import com.example.geopagos2020.utils.Status
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.android.ext.android.inject

class IssuerFragment : Fragment() {

    private val viewModel: IssuerViewModel by inject()
    private lateinit var adapter: IssuerAdapter
    private var data: PaymentData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { bundle ->
            if (bundle.containsKey("DATA")) {
                data = bundle.getParcelable("DATA")
            }
        }
        data?.let {
            viewModel.init(getString(R.string.publicKey), it.paymentMethodId.orEmpty())
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getIssuer().observe(viewLifecycleOwner, Observer { ob ->

            when (ob.status) {
                Status.SUCCESS -> {
                    ob.data?.let {
                        progressBar.visibility = View.GONE
                        adapter = IssuerAdapter(requireActivity(), it)
                        adapter.setOnClick(object : IssuerAdapter.IOnClickListener {
                            override fun OnItemClick(position: Int) {
                                data?.issuerId = it[position].id.toString()
                                data?.issuerName = it[position].name.toString()
                                val bundle = bundleOf("DATA" to data)
                                findNavController().navigate(
                                    R.id.action_issuerFragment_to_fragmentRoot,
                                    bundle
                                )
                            }
                        })
                        rv_list.setHasFixedSize(true)
                        rv_list.layoutManager = LinearLayoutManager(activity)
                        rv_list.adapter = adapter
                    }

                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    rv_list.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        "Error on loading Issuers",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }
}