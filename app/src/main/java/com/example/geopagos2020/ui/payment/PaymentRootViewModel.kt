package com.example.geopagos2020.ui.payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.geopagos2020.model.preference.*
import com.example.geopagos2020.model.util.PaymentData
import com.example.geopagos2020.repository.PaymentRepository
import com.example.geopagos2020.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PaymentRootViewModel(private val paymentRepository: PaymentRepository) : ViewModel() {

    private val paymentLiveData = MutableLiveData<Resource<PreferenceResponse>>()
    private val compositeDisposable = CompositeDisposable()

    fun makePayment(token: String, data: PaymentData?) {
        //TODO delete mock
        val item = Item(
            "My product",
            "Product Description",
            1,
            "ARS",
            data?.amount?.toDouble() ?: 1.0
        )
        val items = listOf(item)

        val payer = Payer(
            "John",
            "Doe",
            "john.doe@test.com"
        )

        val preferencePaymentMethods = PreferencePaymentMethods(
            data?.paymentMethodId.orEmpty(),
            data?.installment?.toInt() ?: 1
        )

        val preference = Preference(items, payer, preferencePaymentMethods)
        postPreference(token, preference)
    }

    private fun postPreference(token: String, preference: Preference) {
        paymentLiveData.postValue(Resource.loading(null))
        compositeDisposable.add(
            paymentRepository.postPreference(token, preference)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ paymentMethod ->
                    paymentLiveData.postValue(Resource.success(paymentMethod))
                }, { throwable ->
                    paymentLiveData.postValue(Resource.error(null, "Something Went Wrong"))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getPaymentResponse(): LiveData<Resource<PreferenceResponse>> {
        return paymentLiveData
    }
}
