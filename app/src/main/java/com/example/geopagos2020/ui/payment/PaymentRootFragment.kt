package com.example.geopagos2020.ui.payment

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.geopagos2020.R
import com.example.geopagos2020.databinding.FragmentPaymentRootBinding
import com.example.geopagos2020.model.preference.PreferenceResponse
import com.example.geopagos2020.model.util.PaymentData
import com.example.geopagos2020.utils.Status
import com.mercadopago.android.px.core.MercadoPagoCheckout
import org.koin.android.ext.android.inject


class PaymentRootFragment : Fragment() {

    private val viewModel: PaymentRootViewModel by inject()
    private lateinit var binding: FragmentPaymentRootBinding
    private var data: PaymentData? = PaymentData(
        null,
        null,
        null,
        null,
        null,
        null
    )

    override fun onAttach(context: Context) {
        super.onAttach(context)
        initBinding()
    }

    private fun initBinding() {
        arguments?.let { bundle ->
            if (bundle.containsKey("DATA")) {
                data = bundle.getParcelable("DATA")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_payment_root, container, false)
        binding.setVariable(BR.data, data)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInit()

        viewModel.getPaymentResponse().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    startPayment(it.data)
                }
                Status.LOADING -> {
                }
                Status.ERROR -> {
                    toast("Error en la carga de las preferencias")
                }
            }
        })

        val bundle = bundleOf("DATA" to data)

        view.findViewById<Button>(R.id.bt_payment_method).setOnClickListener {
            data.let {
                if (!it?.amount.isNullOrBlank()) {
                    findNavController().navigate(
                        R.id.action_fragmentRoot_to_paymentMethodFragment,
                        bundle
                    )
                } else {
                    toast("debe agregar el monto")
                }
            }
        }

        view.findViewById<Button>(R.id.bt_issuer).setOnClickListener {
            data.let {
                if (!it?.paymentMethodId.isNullOrBlank()) {
                    findNavController().navigate(R.id.action_fragmentRoot_to_issuerFragment, bundle)
                } else {
                    toast("debe agregar el método de pago")
                }
            }

        }

        view.findViewById<Button>(R.id.bt_installments).setOnClickListener {
            data.let {
                if (!it?.issuerId.isNullOrBlank()) {
                    findNavController().navigate(
                        R.id.action_fragmentRoot_to_installmentFragment,
                        bundle
                    )
                } else {
                    toast("debe ingresar el emisor")
                }
            }
        }

        view.findViewById<Button>(R.id.bt_make_payment).setOnClickListener {
            data.let {
                if (!it?.installment.isNullOrBlank()) {
                    viewModel.makePayment(getString(R.string.accessToken), it)
                }
            }
        }

    }

    private fun startPayment(data: PreferenceResponse?) {
        MercadoPagoCheckout.Builder(getString(R.string.publicKey), data?.id.orEmpty()).build()
            .startPayment(requireActivity(), MainActivity.MP_REQUEST_CODE)
    }

    private fun setInit() {
        binding.etEnterAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrBlank()) {
                    data?.amount = p0.toString()
                }
            }

            override fun afterTextChanged(p0: Editable?) {}
        })
    }

}

// Extension function to show toast message
fun PaymentRootFragment.toast(message: String) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
}