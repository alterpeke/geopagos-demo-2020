package com.example.geopagos2020.ui.installment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.geopagos2020.R
import com.example.geopagos2020.model.PayerCost
import kotlinx.android.synthetic.main.item_installment.view.*

class InstallmentAdapter(
    private val context: Context,
    private val installments: ArrayList<PayerCost>
) : RecyclerView.Adapter<InstallmentAdapter.ViewHolder>() {

    private lateinit var onClickListener: IOnClickListener

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val instalment: TextView = itemView.tv_nameInstallment
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.instalment.text = installments[position].recommended_message
        holder.instalment.setOnClickListener {
            onClickListener.OnItemClick(position)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_installment, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return installments.size
    }

    fun setOnClick(onClick: IOnClickListener) {
        onClickListener = onClick
    }

    interface IOnClickListener {
        fun OnItemClick(position: Int)
    }

}