package com.example.geopagos2020.ui.issuer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.geopagos2020.model.Issuer
import com.example.geopagos2020.repository.PaymentRepository
import com.example.geopagos2020.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class IssuerViewModel(private val paymentRepository: PaymentRepository) : ViewModel() {

    private val issuerLiveData = MutableLiveData<Resource<ArrayList<Issuer>>>()
    private val compositeDisposable = CompositeDisposable()

    fun init(publickey: String, paymentMethod: String) {
        fetchIssuers(publickey, paymentMethod)
    }

    private fun fetchIssuers(publickey: String, paymentMethod: String) {
        issuerLiveData.postValue(Resource.loading(null))
        compositeDisposable.add(
            paymentRepository.getIssuer(publickey, paymentMethod)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ paymentMethod ->
                    issuerLiveData.postValue(Resource.success(paymentMethod))
                }, { throwable ->
                    issuerLiveData.postValue(Resource.error(null, "Something Went Wrong"))
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getIssuer(): LiveData<Resource<ArrayList<Issuer>>> {
        return issuerLiveData
    }

}