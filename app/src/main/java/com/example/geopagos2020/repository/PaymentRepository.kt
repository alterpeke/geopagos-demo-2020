package com.example.geopagos2020.repository

import com.example.geopagos2020.api.ApiHelper
import com.example.geopagos2020.model.Installment
import com.example.geopagos2020.model.Issuer
import com.example.geopagos2020.model.PaymentMethod
import com.example.geopagos2020.model.preference.Preference
import com.example.geopagos2020.model.preference.PreferenceResponse
import io.reactivex.Single

class PaymentRepository(private val apiHelper: ApiHelper) {

    fun getPaymentMethods(token: String): Single<ArrayList<PaymentMethod>> {
        return apiHelper.getPaymentMethods(token)
    }

    fun getIssuer(publicKey: String, paymentMethod: String): Single<ArrayList<Issuer>> {
        return apiHelper.getIssuer(publicKey, paymentMethod)
    }

    fun getInstallment(
        publicKey: String,
        amount: String,
        paymentMethodId: String,
        issuerId: String
    ): Single<ArrayList<Installment>> {
        return apiHelper.getInstallment(publicKey, amount, paymentMethodId, issuerId)
    }

    fun postPreference(token: String, preference: Preference): Single<PreferenceResponse>{
        return apiHelper.postPreference(token, preference)
    }

}