package com.example.geopagos2020.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}