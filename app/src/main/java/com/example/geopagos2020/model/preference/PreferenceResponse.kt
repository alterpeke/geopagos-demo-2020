package com.example.geopagos2020.model.preference

import com.google.gson.annotations.SerializedName

data class PreferenceResponse(
    @SerializedName("id") var id: String
)