package com.example.geopagos2020.model

import com.google.gson.annotations.SerializedName

data class PayerCost(
    @SerializedName("installments") val installments : Int,
    @SerializedName("installment_rate") val installment_rate : Double,
    @SerializedName("discount_rate") val discount_rate : Double,
    @SerializedName("reimbursement_rate") val reimbursement_rate : String,
    @SerializedName("labels") val labels : List<String>,
    @SerializedName("installment_rate_collector") val installment_rate_collector : List<String>,
    @SerializedName("min_allowed_amount") val min_allowed_amount : Double,
    @SerializedName("max_allowed_amount") val max_allowed_amount : Double,
    @SerializedName("recommended_message") val recommended_message : String,
    @SerializedName("installment_amount") val installment_amount : Double,
    @SerializedName("total_amount") val total_amount : Double,
    @SerializedName("payment_method_option_id") val payment_method_option_id : String
)