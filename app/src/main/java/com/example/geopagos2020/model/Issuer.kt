package com.example.geopagos2020.model

import com.google.gson.annotations.SerializedName

data class Issuer(
    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("secure_thumbnail") val secure_thumbnail : String,
    @SerializedName("thumbnail") val thumbnail : String,
    @SerializedName("processing_mode") val processing_mode : String,
    @SerializedName("merchant_account_id") val merchant_account_id : String
)