package com.example.geopagos2020.model.preference

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("title") val title: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("quantity") val quantity: Int? = null,
    @SerializedName("currency_id") val currencyId: String? = null,
    @SerializedName("unit_price") val unitPrice: Double? = null
)