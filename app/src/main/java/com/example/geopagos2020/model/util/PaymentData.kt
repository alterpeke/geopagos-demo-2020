package com.example.geopagos2020.model.util

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentData(
    var amount: String?,
    var paymentMethodId: String?,
    var issuerId: String?,
    var issuerName: String?,
    var installmentTitle: String?,
    var installment: String?
) : Parcelable