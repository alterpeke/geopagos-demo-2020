package com.example.geopagos2020.model.preference

import com.google.gson.annotations.SerializedName

data class Payer(
    @SerializedName("name") var name: String? = null,
    @SerializedName("surname") val surname: String? = null,
    @SerializedName("email") var email: String? = null
)