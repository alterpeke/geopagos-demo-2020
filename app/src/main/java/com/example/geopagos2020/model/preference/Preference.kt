package com.example.geopagos2020.model.preference

import com.google.gson.annotations.SerializedName

data class Preference(
    @SerializedName("items") var items: List<Item>,
    @SerializedName("payer") val payer: Payer,
    @SerializedName("payment_methods") val paymentMethods: PreferencePaymentMethods
)