package com.example.geopagos2020.model

import com.google.gson.annotations.SerializedName

data class Installment(
    @SerializedName("payment_method_id") val paymentMethodId : String,
    @SerializedName("payment_type_id") val paymentTypeId : String,
    @SerializedName("issuer") val issuer : Issuer,
    @SerializedName("processing_mode") val processing_mode : String,
    @SerializedName("merchant_account_id") val merchantAccountId: String,
    @SerializedName("payer_costs") val payerCosts : ArrayList<PayerCost>,
    @SerializedName("agreements") val agreements : String
)
