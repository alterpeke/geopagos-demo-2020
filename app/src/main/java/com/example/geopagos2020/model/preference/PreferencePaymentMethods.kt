package com.example.geopagos2020.model.preference

import com.google.gson.annotations.SerializedName

data class PreferencePaymentMethods(
    @SerializedName("id") var id: String,
    @SerializedName("installments") val installments: Int
)