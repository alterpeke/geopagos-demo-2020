package com.example.geopagos2020.api

import com.example.geopagos2020.model.preference.Preference

class ApiHelper(private val apiService: ApiService) {

    fun getPaymentMethods(token: String) = apiService.getPaymentMethods(token)

    fun getIssuer(publicKey: String, paymentMethod: String) =
        apiService.getIssuers(publicKey, paymentMethod)

    fun getInstallment(
        publicKey: String,
        amount: String,
        paymentMethodId: String,
        issuerId: String
    ) = apiService.getInstallments(publicKey, amount, paymentMethodId, issuerId)

    fun postPreference(token: String, preference: Preference) =
        apiService.postPreference(token, preference)

}