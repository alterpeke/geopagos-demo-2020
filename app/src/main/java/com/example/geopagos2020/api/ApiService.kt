package com.example.geopagos2020.api

import com.example.geopagos2020.model.Installment
import com.example.geopagos2020.model.Issuer
import com.example.geopagos2020.model.PaymentMethod
import com.example.geopagos2020.model.preference.Preference
import com.example.geopagos2020.model.preference.PreferenceResponse
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import java.util.*

interface ApiService {

    // Payment Method
    @GET("v1/payment_methods")
    fun getPaymentMethods(@Query("access_token") token: String?): Single<ArrayList<PaymentMethod>>

    //Issuers
    @GET("v1/payment_methods/card_issuers")
    fun getIssuers(
        @Query("public_key") publicKey: String?,
        @Query("payment_method_id") paymentMethodId: String?
    ): Single<ArrayList<Issuer>>

    //Instalments
    @GET("v1/payment_methods/installments")
    fun getInstallments(
        @Query("public_key") publicKey: String?,
        @Query("amount") amount: String?,
        @Query("payment_method_id") paymentMethodId: String?,
        @Query("issuer.id") issuerId: String?
    ): Single<ArrayList<Installment>>

    //Preference
    @POST("checkout/preferences")
    fun postPreference(
        @Query("access_token") token: String?,
        @Body preference: Preference?
    ): Single<PreferenceResponse>

}