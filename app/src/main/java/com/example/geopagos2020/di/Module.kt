package com.example.geopagos2020.di

import com.example.geopagos2020.api.ApiHelper
import com.example.geopagos2020.api.ApiService
import com.example.geopagos2020.repository.PaymentRepository
import com.example.geopagos2020.ui.installment.InstallmentViewModel
import com.example.geopagos2020.ui.issuer.IssuerViewModel
import com.example.geopagos2020.ui.method.PaymentMethodViewModel
import com.example.geopagos2020.ui.payment.PaymentRootViewModel
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://api.mercadopago.com/"

private val loggingInterceptor = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}

private val client = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()

private fun getRetrofit(): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

val apiService: ApiService = getRetrofit().create(ApiService::class.java)

val appModule = module {

    single { apiService }
    single { ApiHelper(apiService = get()) }
    single { PaymentRepository(apiHelper = get())}
    viewModel { PaymentMethodViewModel(paymentRepository = get()) }
    viewModel { IssuerViewModel(paymentRepository = get()) }
    viewModel { InstallmentViewModel(paymentRepository = get()) }
    viewModel { PaymentRootViewModel(paymentRepository = get()) }

}


